from flask import Flask , jsonify ,request
from elasticsearch import Elasticsearch

app = Flask(__name__)

#Connection for Elastic Search
es = Elasticsearch(HOST="http://localhost", PORT=9200)
es = Elasticsearch()

#Endpoint to check the connection 
@app.route("/")
def hello_world():
  return "<p>From Service_2</p>"

#Endpoint to insert document into database
@app.route("/set_data" , methods = ['POST'])
def set_data():

	if( request.method == "POST" ):

		req_data = request.json
		unique_id = req_data['product']['name']
		
		try:
			es.index(index="product_specification", doc_type="product_data", id=unique_id, body=req_data)
    
		except:
			return jsonify({"response" : "Error in data Connection"})
	
		return jsonify({"response" : "successfully inserted in db"})

	else :
		return jsonify({ "response" : "Invalid request type" } )

if __name__ == "__main__":
  app.run( debug =True , host='0.0.0.0' )

#Improvement that can be done
#1.The payload should come from service_1
