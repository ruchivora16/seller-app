import requests

#Used for Web Scrapping to get the data from HTML
from bs4 import BeautifulSoup

#Used for Creating Web API
from flask import Flask,jsonify,request

app = Flask(__name__)

#Endpoint to check the connection 
@app.route("/")
def hello_world():
  return "<p>From Service_1</p>"

#Endpoint that scrapes the Webpage
@app.route("/webscraper")
def web_scrapper():

  default_url = "https://www.mirraw.com/designers/geroo-jaipur/designs/pink-hand-crafted-bandhani-lehenga-set-ghagra-choli"

  scrape_url = request.args.get("scrape_url")
  
  if scrape_url is None:
    scrape_url = default_url
  
  try:
    
    #Get the content of the Webpage
    r = requests.get(scrape_url)
    html_content = r.content

    #Converting the webpage content in readable form 
    soup = BeautifulSoup( html_content , 'html.parser' )

    #Extracting data from the html content of the webpage
    product_name   = soup.find( 'h1', { "id" : "design_title"} ).get_text()
    product_price  = soup.find( 'h3', { "class" : "new_price_label"} ).get_text()
    product_data   = soup.find( 'div', { "id" : "spec-1"} )
    product_data   = product_data.tbody.find_all('tr')[1]
    product_desc   = product_data.find('p').get_text()
    product_rating = soup.find( 'div' , class_ = "review-count" ).get_text()
    product_image  = soup.find( 'div' , {"id" : "gal1"} )
    product_image  = product_image.a.attrs['data-image'] 
  
  except : 
    result = { "error" : "Invalid URL" }
    return jsonify( result )

  #Formatting the data in required format
  result = {
    "url" : scrape_url ,
    "product" : {
      "name"  : product_name,
      "price" : product_price,
      "description"  : product_desc,
      "image" : product_image
    }
  }
  
  # print( "before post" )
  # requests.post("http://localhost:5000/set_data" , data=result )

  return jsonify(result)

if __name__ == "__main__":
  app.run( debug =True , host='0.0.0.0' )

#Improvement that can be done
#1. Send the JSON output obtained after scraping the Webpage to service_2
#2. Extract more useful data like (if the product is Ready To Ship or not ) 