### Steps to check Service 1
1. sudo docker-compose up service_1
2. Input to service 1 (API Specification) : Http request type = Get
                         End point = http://localhost:5000/webscraper
                         Parameters = scrape_url
 Note : For scrapping the data you can give any url of product details page of Mirraw website
 Eg : https://www.mirraw.com/designers/yash-enterprise/designs/green-embroidered-satin-with-blouse-stylish-lahengha-choli-lehenga-choli--3ca19c5a-23d2-4564-8afb-e160402002f6 

3. The response(output) will be the scraped data in json form 
   Eg : {
    "product": {
        "description": "Top Fabric: Faux Georgette , Bottom Material: Santoon, Dupatta: Nazneen, Inner: Santoon , Weight: 450 Gm, Product Size: Free Size, Chest Size: Max. Up To 44\" , Length: Max. Up To 55\" , Bottom: Up To 2.0 Mtr , Dupatta : Up To 2.2 Mtr , Dress Type: Heavy Embroidery Anarkali Gown ,Occasion: Festival, Bridal, Party , Casual Wear , Recommended Fabric Care : Dry Wash ,Hand Wash, Machin Wash , Disclaimer: Color Of Product May Slightly Vary Due To Digital Photography Or Your Monitor/Mobile Settings.",
        "image": "https://assets0.mirraw.com/images/8352155/87A_large.jpg?1602391529",
        "name": "White embroidered faux georgette semi stitched anarkali salwar with dupatta",
        "price": "Rs 2609"
    },
    "url": "https://www.mirraw.com/designers/divine-international-trading-co/designs/white-embroidered-faux-georgette-semi-stitched-anarkali-salwar-with-dupatta-anarkali-salwar-kameez--91468327-8ec8-4402-a64b-fc543e1399f8"
}


### Steps to check Service 2
The service 2 writes the data into document Store i.e Elastic search

1. start elastic search by  : sudo docker-compose up elasticsearch
2. start service 2 : pip install -r requirements.txt
3. Input to service 2 (API specification) : Http request type = Post
                                            End Point = http://localhost:5000/set_data
                                            Parameters = The json response obtained from service_1
4. The response will be json , that gives success if document is stored in elastic search 


